import json
import sys

FILENAME = 'todos.json'


class TodoApp():

    def __init__(self):
        self._storage = []

    def save(self):
        storage_data = json.dumps(self._storage, ensure_ascii=False)
        file = open(FILENAME, 'w')
        file.write(storage_data)
        file.close()

    def load(self):
        file = open(FILENAME, 'r')
        storage_data = file.read()
        file.close()
        self._storage = json.loads(storage_data)

    def add(self, new_data: str) -> int:
        self._storage.append(new_data)
        return len(self._storage) - 1

    def get_data(self) -> list:
        return self._storage

    def delete(self, id: int) -> list:
       if 0 <= id < len(self._storage):
            self._storage.pop(id)
        

if __name__ == "__main__":
    todo_app = TodoApp()
    todo_app.load()

    if len(sys.argv) == 1:
        print("Error")          
    elif sys.argv[1] == "get_data":
        print(todo_app.get_data())  
    elif sys.argv[1] == "add" and len(sys.argv) >= 3: 
        todo_app.add(sys.argv[2])
    elif sys.argv[1] == "delete" and len(sys.argv) == 3:
        todo_app.delete(int(sys.argv[2]))

    todo_app.save()